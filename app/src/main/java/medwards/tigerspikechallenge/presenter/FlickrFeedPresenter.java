package medwards.tigerspikechallenge.presenter;

import android.os.AsyncTask;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;
import medwards.tigerspikechallenge.model.entity.FlickrFeed;
import medwards.tigerspikechallenge.model.entity.Item;
import medwards.tigerspikechallenge.view.FlickrFeedView;

import java.io.IOException;
import java.net.URL;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class FlickrFeedPresenter extends MvpBasePresenter<FlickrFeedView> {

    private static final ObjectMapper JSON_MAPPER = new ObjectMapper().configure(JsonParser.Feature.ALLOW_BACKSLASH_ESCAPING_ANY_CHARACTER, true);
    private static final String FLICKR_FEED_URL = "http://api.flickr.com/services/feeds/photos_public.gne?format=json&nojsoncallback=true";

    private int index;
    private List<Item> items;
    private FlickrItemsTask task;

    public FlickrFeedPresenter() {
        task = new FlickrItemsTask();
        items = Collections.emptyList();
    }

    public void onLoaded() {
        if (isViewAttached()) {
            getView().beginDisplay();
        }
    }

    public Item getNextItem() throws FlickrFeedException {

        try {

            if (index == items.size()) {
                //We have run out of items: get the next ones from the task
                items = task.get();
                index = 0;
            } else if (index == items.size() / 2) {
                //We are running out of items: start a new task
                task = new FlickrItemsTask();
            }

            return items.get(index++);

        } catch (InterruptedException | ExecutionException e) {
            if (isViewAttached()) {
                getView().handleError();
            }
            throw new FlickrFeedException(e);
        }

    }

    private class FlickrItemsTask extends AsyncTask<Void, Void, List<Item>> {

        /**
         * Creates and begins the execution of an AsyncTask which retreives the next set of Flickr feed info
         */
        FlickrItemsTask() {
            super();
            execute();
        }

        @Override
        protected List<Item> doInBackground(Void... params) {

            try {
                URL flickrUrl = new URL(FLICKR_FEED_URL);
                return JSON_MAPPER.readValue(flickrUrl, FlickrFeed.class).getItems();
            } catch (IOException ioe) {
                ioe.printStackTrace();
                cancel(true);
                return null;
            }

        }

        @Override
        protected void onCancelled() {
            if (isViewAttached()) {
                getView().handleError();
            }
        }

    }
}
