package medwards.tigerspikechallenge.presenter;

/**
 * Author: Michael Edwards<p>
 * Date: 18/10/2016<p>
 * Exception to be thrown whenever a problem is encountered obtaining data from the Flickr feed
 */
public class FlickrFeedException extends Exception {

    FlickrFeedException(Throwable throwable) {
        super(throwable);
    }

}
