package medwards.tigerspikechallenge.view;

import com.hannesdorfmann.mosby.mvp.MvpView;

/**
 * Created by SYSDEV5E on 24/10/2016.
 */

//Define what a view using this presenter should do
public interface FlickrFeedView extends MvpView {

    void beginDisplay();

    void handleError();

}