package medwards.tigerspikechallenge.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.hannesdorfmann.mosby.mvp.MvpActivity;
import medwards.tigerspikechallenge.R;
import medwards.tigerspikechallenge.model.entity.Item;
import medwards.tigerspikechallenge.presenter.FlickrFeedException;
import medwards.tigerspikechallenge.presenter.FlickrFeedPresenter;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static medwards.tigerspikechallenge.view.FlickrItemFragment.*;

public class FlickrFeedActivity extends MvpActivity<FlickrFeedView, FlickrFeedPresenter> implements FlickrFeedView {

    private LayoutInflater inflater;

    @BindView(R.id.layout_main)
    LinearLayout mainLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        inflater = LayoutInflater.from(this);
//        presenter = new FlickrFeedPresenter(this);getPres
//        presenter.onLoaded();
        getPresenter().onLoaded();

    }

    @NonNull
    @Override
    public FlickrFeedPresenter createPresenter() {
        return new FlickrFeedPresenter();
    }

    @Override
    public void beginDisplay() {

        ViewPager viewPager = (ViewPager) inflater.inflate(R.layout.activity_main_viewpager, mainLayout, false);
        viewPager.setAdapter(new FlickrFeedAdapter());
        mainLayout.addView(viewPager);

    }

    @Override
    public void handleError() {

        mainLayout.removeAllViews();
        inflater.inflate(R.layout.activity_main_loading_failed, mainLayout, true);

    }

    private class FlickrFeedAdapter extends FragmentPagerAdapter {

        private final String EMPTY_TITLE = "(No Title)";
        private final DateFormat DATE_FORMAT_FLICKR = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss", Locale.UK);
        private final DateFormat DATE_FORMAT_OUTPUT = new SimpleDateFormat("'Taken on 'EEE, MMM d yyyy 'at' h:mm a", Locale.UK);

        FlickrFeedAdapter() {
            super(getSupportFragmentManager());
        }

        @Override
        public Fragment getItem(int position) {

            FlickrItemFragment fragment = new FlickrItemFragment();

            try {

                Item item = presenter.getNextItem();

                String title = item.getTitle().trim().isEmpty() ? EMPTY_TITLE : item.getTitle();
                String author = item.getAuthor();
                String tags = item.getTags();
                Date flickrDate = DATE_FORMAT_FLICKR.parse(item.getDateTaken());
                String dateTaken = DATE_FORMAT_OUTPUT.format(flickrDate);

                Bundle args = new Bundle();
                args.putString(FLICKR_ITEM_IMG_URL, item.getMedia().getM());
                args.putString(FLICKR_ITEM_TITLE, title);
                args.putString(FLICKR_ITEM_DATE_TAKEN, dateTaken);
                args.putString(FLICKR_ITEM_AUTHOR, author);
                args.putString(FLICKR_ITEM_TAGS, tags);

                fragment.setArguments(args);

                return fragment;

            } catch (FlickrFeedException | ParseException e) {
                e.printStackTrace();
                return fragment;
            }

        }

        @Override
        public int getCount() {
            return Integer.MAX_VALUE;
        }

    }
}

