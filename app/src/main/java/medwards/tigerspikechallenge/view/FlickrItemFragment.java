package medwards.tigerspikechallenge.view;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.squareup.picasso.Picasso;
import medwards.tigerspikechallenge.R;

/**
 * Author: Michael Edwards<p>
 * Date: 11/10/2016<p>
 * Fragment which contains information from the Flickr feed
 */
public class FlickrItemFragment extends Fragment {

    static final String FLICKR_ITEM_AUTHOR = "author";
    static final String FLICKR_ITEM_TITLE = "title";
    static final String FLICKR_ITEM_TAGS = "tags";
    static final String FLICKR_ITEM_IMG_URL = "img";
    static final String FLICKR_ITEM_DATE_TAKEN = "date";

    @BindView(R.id.image_flickr_item)
    ImageView image;

    @BindView(R.id.text_flickr_item_author)
    TextView author;

    @BindView(R.id.text_flickr_item_date)
    TextView date;

    @BindView(R.id.text_flickr_item_title)
    TextView title;

    @BindView(R.id.text_flickr_item_tags)
    TextView tags;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View fragmentView = inflater.inflate(R.layout.fragment_flickr_item, container, false);
        Bundle args = getArguments();

        if (args != null) {

            ButterKnife.bind(this, fragmentView);
            String authorString = args.getString(FLICKR_ITEM_AUTHOR);
            String dateString = args.getString(FLICKR_ITEM_DATE_TAKEN);
            String titleString = args.getString(FLICKR_ITEM_TITLE);
            String tagsString = args.getString(FLICKR_ITEM_TAGS);

            Picasso.with(getContext())
                    .load(args.getString(FLICKR_ITEM_IMG_URL))
                    .into(image);

            author.setText(authorString);
            date.setText(dateString);
            title.setText(titleString);
            tags.setText(tagsString);

        }

        return fragmentView;

    }

}